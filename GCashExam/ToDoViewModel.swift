//
//  ToDoViewModel.swift
//  GCashExam
//
//  Created by Neil Francis Hipona on 7/31/21.
//

import Foundation

class ToDoViewModel {
    
    // only support current month
    struct ToDoModel: Codable {
        var user: User
        var todo: [CVDate]
    }
    
    struct CVDate: Codable {
        var date: Date
        var todoStr: String?
    }
    
    private var currentUser: User!
    
    convenience init(user: User) {
        self.init()
        currentUser = user
    }
    
    lazy var calendar: Calendar = {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        return calendar
    }()
    
    lazy var formatter: DateFormatter = {
       var formatter = DateFormatter()
        formatter.calendar = calendar
        return formatter
    }()
    
    lazy var daysOfMonth: [CVDate] = {
        if let userTodo = loadUserTodo(user: currentUser) {
            return userTodo.todo
        }
        
        let currentDate = Date()
        let cMonthYear = calendar.dateComponents([.month, .year], from: currentDate)
        let startOfMonth = calendar.date(from: cMonthYear)!
        
        let nMonthYear = DateComponents(month: 1, day: -1)
        let endOfMonth = calendar.date(byAdding: nMonthYear, to: startOfMonth)!
        print("currentDate: \(currentDate) -- startOfMonth: \(startOfMonth) -- endOfMonth: \(endOfMonth)")
        
        var currentDay = startOfMonth
        var dateCollections: [CVDate] = []
        
        while currentDay < endOfMonth {
            let cvDate = CVDate(date: currentDay, todoStr: nil)
            dateCollections.append(cvDate)
            
            let nextDayComponent = DateComponents(day: 1)
            currentDay = calendar.date(byAdding: nextDayComponent, to: currentDay)!
        }
        
        return dateCollections
    }()
}

extension ToDoViewModel {
    func dayFrom(date: Date) -> String {
        formatter.dateFormat = "dd"
        return formatter.string(from: date)
    }
    
    func dayNameFrom(date: Date) -> String {
        formatter.dateFormat = "EEE"
        return formatter.string(from: date)
    }
    
    func dateNameFrom(date: Date) -> String {
        formatter.dateFormat = "EEE MM/dd"
        return formatter.string(from: date)
    }
    
    func updateToDo(forDate date: Date, withStr str: String?) {
        daysOfMonth = daysOfMonth.map { cvDate in
            if cvDate.date == date {
                return CVDate(date: date, todoStr: str)
            }
            return cvDate
        }
    }
}

extension ToDoViewModel {
    
    func saveUserTodo() {
        findUserTodo(user: currentUser)
    }
    
    func loadUserTodo(user: User) -> ToDoModel? {
        if let todoData = UserDefaults.standard.value(forKey: "users_todo") as? Data,
           let users = try? PropertyListDecoder().decode([ToDoModel].self, from: todoData) {
            let foundUser = users.first { u in
                return u.user.username == user.username
            }
            
            if let foundUser = foundUser {
                print("User todo found:", foundUser)
                return foundUser
            }
        }
        
        return nil
    }
    
    func findUserTodo(user: User) {
        if let todoData = UserDefaults.standard.value(forKey: "users_todo") as? Data,
           let todos = try? PropertyListDecoder().decode([ToDoModel].self, from: todoData) {
            let foundUser = todos.first { u in
                return u.user.username == user.username
            }
            
            if let foundUser = foundUser {
                print("User found:", foundUser)
                updateUserTodo(todo: foundUser, collection: todos)
            }
        }
        
        saveUserTodo(todo: ToDoModel(user: user, todo: daysOfMonth))
    }
    
    func saveUserTodo(todo: ToDoModel) {
        let collection: [ToDoModel] = [todo]
        let userData = try? PropertyListEncoder().encode(collection)
        UserDefaults.standard.setValue(userData, forKey: "users_todo")
        UserDefaults.standard.synchronize()
    }
    
    func updateUserTodo(todo: ToDoModel, collection: [ToDoModel] = []) {
        let collectionTmp = collection.map { n -> ToDoModel in
            if n.user.username == todo.user.username {
                return ToDoModel(user: todo.user, todo: daysOfMonth)
            }
            return todo
        }
        
        let userData = try? PropertyListEncoder().encode(collectionTmp)
        UserDefaults.standard.setValue(userData, forKey: "users_todo")
        UserDefaults.standard.synchronize()
    }
    
    func exportToDo() {
        let todoData = ToDoModel(user: currentUser, todo: daysOfMonth)
        guard let jsonData = try? JSONEncoder().encode(todoData),
              let jsonString = String(data: jsonData, encoding: .utf8)
        else {
            print("Export TO DO failed: Can't convert data")
            return
        }
        
        exportJSON(todoJSON: jsonString)
    }
    
    func exportJSON(todoJSON: String) {
        if let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                            in: .userDomainMask).first {
            let fileName = "\(currentUser.username)-ToDoJSON.json"
            let pathWithFilename = documentDirectory.appendingPathComponent(fileName)
            do {
                try todoJSON.write(to: pathWithFilename,
                                     atomically: true,
                                     encoding: .utf8)
                print("Exported TO DO: \(todoJSON) -- in: \(pathWithFilename)")
            } catch {
                print("Export TO DO failed:", error)
            }
        }
    }
}
