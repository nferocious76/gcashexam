//
//  ViewController.swift
//  GCashExam
//
//  Created by Neil Francis Hipona on 7/31/21.
//

import UIKit
import RxSwift
import RxCocoa
import RxRelay

class LoginViewController: UIViewController {
    
    @IBOutlet weak var uTxtField: UITextField!
    @IBOutlet weak var pwTxtField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    lazy var viewModel: LoginViewModel = { LoginViewModel() }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "Login"
        pwTxtField.isSecureTextEntry = true
        _ = loginBtn.rx.controlEvent(.touchUpInside).subscribe(onNext: { [unowned self] in
            viewModel.setUsernameAndPassword(username: uTxtField.text!, password: pwTxtField.text!)
            viewModel.validate { success, error in
                if let error = error as NSError? {
                    showAlert(withError: error)
                }else{
                    todo()
                }
            }
        })
    }
    
    func showAlert(withError error: NSError) {
        let msg = error.userInfo["message"] as? String ?? "Unkown Error"
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func todo() {
        guard let todoVC = storyboard?.instantiateViewController(identifier: "ToDoViewController") as? ToDoViewController else { return }
        todoVC.user = viewModel.user
        navigationController?.pushViewController(todoVC, animated: true)
    }
}

