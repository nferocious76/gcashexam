//
//  ToDoViewController.swift
//  GCashExam
//
//  Created by Neil Francis Hipona on 7/31/21.
//

import Foundation
import UIKit

class ToDoViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var cellSize: CGSize = {
        let screenWidth = UIScreen.main.bounds.width - 6
        let cellWidth = screenWidth / 7.0
        return CGSize(width: cellWidth, height: 83)
    }()
    
    var user: User!
    lazy var viewModel: ToDoViewModel = { ToDoViewModel(user: user) }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "TO DO"
        
        let cvFlowlayout = UICollectionViewFlowLayout()
        cvFlowlayout.itemSize = cellSize
        cvFlowlayout.estimatedItemSize = .zero
        cvFlowlayout.minimumInteritemSpacing = 1
        cvFlowlayout.minimumLineSpacing = 1
        collectionView.collectionViewLayout = cvFlowlayout
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let barButtons: [UIBarButtonItem] = [
            UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveToDo)),
            UIBarButtonItem(title: "Export", style: .plain, target: self, action: #selector(exportToDo))
        ]
        navigationItem.rightBarButtonItems = barButtons
    }
    
    @objc func saveToDo() {
        viewModel.saveUserTodo()
    }
    
    @objc func exportToDo() {
        viewModel.exportToDo()
    }
}

extension ToDoViewController {
    func showAlert(withDate date: ToDoViewModel.CVDate) {
        let msg = "TODO for \(viewModel.dateNameFrom(date: date.date))"
        let alert = UIAlertController(title: "TODO", message: msg, preferredStyle: .alert)
        alert.addTextField { tfield in
            tfield.placeholder = "TO TO text..."
            tfield.text = date.todoStr
        }
        
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [unowned self] _ in
            let textField = alert.textFields![0] as UITextField
            viewModel.updateToDo(forDate: date.date, withStr: textField.text!)
            collectionView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { [unowned self] _ in
            viewModel.updateToDo(forDate: date.date, withStr: nil)
            collectionView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension ToDoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.daysOfMonth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return ToDoCVCell.instance(inCV: collectionView, indexPath: indexPath)
    }
}

extension ToDoViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

extension ToDoViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let dateData = viewModel.daysOfMonth[indexPath.item]
        let todoCell = cell as! ToDoCVCell
        todoCell.dayLbl.text = viewModel.dayFrom(date: dateData.date)
        todoCell.monthLbl.text = viewModel.dayNameFrom(date: dateData.date)
        let hasTodo = !(dateData.todoStr ?? "").isEmpty
        todoCell.setHasTodo(hasToDo: hasTodo)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let dateData = viewModel.daysOfMonth[indexPath.item]
        showAlert(withDate: dateData)
    }
}
