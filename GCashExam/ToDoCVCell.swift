//
//  ToDoCVCell.swift
//  GCashExam
//
//  Created by Neil Francis Hipona on 7/31/21.
//

import Foundation
import UIKit

class ToDoCVCell: UICollectionViewCell {
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor.gray.cgColor
        
        setHasTodo(hasToDo: false)
    }
    
    func setHasTodo(hasToDo: Bool) {
        backgroundColor = hasToDo ? .gray : .white
    }
}

extension ToDoCVCell {
    class func instance(inCV cv: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let instance = cv.dequeueReusableCell(withReuseIdentifier: "ToDoCVCell", for: indexPath)
        return instance
    }
}
