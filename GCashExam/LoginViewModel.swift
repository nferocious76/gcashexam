//
//  LoginViewModel.swift
//  GCashExam
//
//  Created by Neil Francis Hipona on 7/31/21.
//

import Foundation

struct User: Codable {
    var username: String
    var password: String
}

class LoginViewModel {
    
    var user: User!
    
    func setUsernameAndPassword(username: String, password: String) {
        user = .init(username: username, password: password)
    }
    
    func validate(completion: @escaping (Bool, Error?) -> Void) {
        if user.username.isEmpty && user.password.isEmpty {
            return completion(false, NSError(domain: "error.GCash", code: -999, userInfo: ["message": "Missing username or password"]))
        }
        
        let success = findUser(user: user)
        let error: Error? = success ? nil : NSError(domain: "error.GCash", code: -999, userInfo: ["message": "Wrong username or password"])
        completion(success, error)
    }
    
    func findUser(user: User) -> Bool {
        if let usersData = UserDefaults.standard.value(forKey: "users") as? Data,
           let users = try? PropertyListDecoder().decode([User].self, from: usersData) {
            let foundUser = users.first { u in
                return u.username == user.username
            }
            
            if let foundUser = foundUser {
                print("User found:", foundUser)
                return foundUser.password == user.password // wrong user password
            }else{
                saveUser(user: user, collection: users)
            }
        }else{
            saveUser(user: user)
        }
        
        return true // new user
    }
    
    func saveUser(user: User, collection: [User] = []) {
        var collectionTmp = collection
        collectionTmp.append(user)
        let userData = try? PropertyListEncoder().encode(collectionTmp)
        UserDefaults.standard.setValue(userData, forKey: "users")
        UserDefaults.standard.synchronize()
        
        print("Users:", collectionTmp)
    }
}
